export HOSTNAME=`hostname` 

export BASE_DIR="/home/tctrl/Long_term_test/web_interface/web_server";
export SERVER_IP="localhost";
cd $BASE_DIR;

export FLASK_APP=main.py
export FLASK_ENV=development
export FLASK_DEBUG=1
export PYTHONPATH=$BASE_DIR/app:$PYTHONPATH
flask run -h $SERVER_IP -p 55555
