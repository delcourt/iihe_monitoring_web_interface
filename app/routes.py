from app import app
from flask import render_template, request
import subprocess
from datetime import datetime, timedelta
import os,sys,math, glob, json
from magnet_db import event, magnet_db
from dummy_db import dummy_db
from zmq_interface import zmq_client
rq = zmq_client(5555)

config_file = "/home/tctrl/DCS/magnet-test-env-monitoring/conf.json"

psu_files_dir = "/home/tctrl/Long_term_test"
rht_files_dir = "/home/tctrl/DCS/magnet-test-env-monitoring/"

psu_naming ={
             "LV1":"CH1 (DUT)",
             "LV2":"CH2",
             "LV3":"CH3",
             "HV1":"DUT",
             "HV2":"N/A",
             "LI1":"CH1 (DUT)",
             "LI2":"CH2",
             "LI3":"CH3",
             "LV4":"N/A",
             "LI4":"N/A",
             "HI1":"DUT",
             "HI2":"N/A"
             }




def time_select_blocks(start,stop):
    now = datetime.now()
    yesterday = now +timedelta(hours=-2)
    months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    data = {}
    for block,tt in [("start",yesterday),("stop",now)]:
        data[block] = f"<select id='{block}_year'><option>2022</option><option>2021</option><option>2020</option></select>"
        data[block]+= f"<select id='{block}_month'>"
        for i in range(12):
            if i+1 == tt.month:
                data[block]+=f"<option value='{i+1}' selected>{months[i]}</option>"
            else:
                data[block]+=f"<option value='{i+1}'>{months[i]}</option>"

        data[block]+=f"</select><select id='{block}_day'>"
        for i in range(31):
            if i+1 == tt.day:
                data[block]+=f"<option value='{i+1}' selected>{i+1:02d}</option>"
            else:
                data[block]+=f"<option value='{i+1}' >{i+1:02d}</option>"

        data[block]+=f"</select><select id='{block}_hour'>"
        for i in range(24):
            if i == tt.hour:
                data[block]+=f"<option value='{i}' selected>{i:02d}h</option>"
            else:
                data[block]+=f"<option value='{i}' >{i:02d}h</option>"
        data[block]+=f"</select><select id='{block}_min'>"
        for i in range(60):
            if i == tt.minute:
                data[block]+=f"<option value='{i}' selected>{i:02d}</option>"
            else:
                data[block]+=f"<option value='{i}'>{i:02d}</option>"
        data[block]+="</select>"
    return data





@app.route('/')
@app.route('/index')

def index():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    fList = sorted(glob.glob(rht_files_dir+'*.bin'), key=os.path.getmtime)
    fList.reverse()
    files = [f.split("/")[-1] for f in fList]
    return render_template('plots.html',start_block = data["start"], stop_block=data["stop"], file_list = files)


@app.route('/plotting_data')
def env_data():
    conf = json.load(open(config_file,"r"))
    debug_message = ""
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()

    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)
    data_file = rht_files_dir+request.args.get('data_file')
    data = {"time":[], "temp" : {}, "hum" : {}}

    db = magnet_db(config_file,data_file)

    #Find starting point :
    n_evt = db.number_of_events
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["timestamp"] < start:
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["timestamp"] < stop:
            entry_number_stop = to_check
    debug_message+=f"Start dt : {db.get_entry(entry_number_start)['timestamp']-start} <br/>"
    debug_message+=f"Stop  dt : {db.get_entry(entry_number_stop)['timestamp']-stop} <br/>"

    #print (debug_message)

    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        if not entry==None:
            for key in entry.data:
                val = entry.data[key]

                if key == "timestamp":
                    data["time"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))
                if not key in conf["meas_to_show"]:
                    continue

                data_type = "None"
                if  "_T" in key or "PT_" in key:
                    data_type = "temp"
                elif  "_H" in key:
                    data_type = "hum"
                if data_type == "None":
                    continue

                if key in conf["alias"].keys():
                    key = conf["alias"][key]


                if not key in data[data_type]:
                    data[data_type][key] = [val]
                else:
                    if data_type == "temp" and val == -50.0:
                        data[data_type][key].append(data[data_type][key][-1])
                    else:
                        data[data_type][key].append(val)

        entry_number += read_every

    #creating plots from data :
    plots = {"temp" : [], "hum" : []}
    for meas_type in ["temp","hum"]:
        for meas in data[meas_type]:
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["time"],
                "type" : "scatter",
                "name" : meas
            })

    return plots
@app.route('/psu')
def psu():
    start = request.args.get('start')
    stop = request.args.get('stop')
    data = time_select_blocks(start,stop)
    #fList = sorted(glob.glob(files_dir+'*.bin'), key=os.path.getmtime)
    #fList.reverse()
    #files = [f.split("/")[-1] for f in fList]
    return render_template('plots_psu.html',start_block = data["start"], stop_block=data["stop"])

def val_converter(meas_type,val):
    if "lv" in meas_type or "hv_v" in meas_type:
        return 0.001*val
    if "hv_i" in meas_type:
        return 1e-9*val


@app.route('/plotting_data_psu')
def plotting_data_psu():
    start = request.args.get('start')
    if not start == None and start.isdigit():
        start = datetime.now().timestamp() - int(start)
    else:
        try:
            start = datetime.strptime(start, '%Y_%m_%d_%H_%M_%S').timestamp()
        except Exception as e:
            start = datetime.now().timestamp() - 2*60*60

    stop = request.args.get('stop')
    try:
        stop = datetime.strptime(stop, '%Y_%m_%d_%H_%M_%S').timestamp()
    except Exception as e:
        stop = datetime.now().timestamp()

    max_points = request.args.get('max_points')
    if max_points == None or not max_points.isdigit():
        max_points = 1000
    else:
        max_points = int(max_points)

    #data_file = files_dir+request.args.get('data_file')
    data_file = psu_files_dir+"/psu.db"
    data = {"date":[], "lv_v" : {}, "lv_i" : {}, "hv_v" : {}, "hv_i" : {}}

    db = dummy_db(data_file)

    #Find starting point :
    n_evt = db.number_of_events
    n_bits = int(math.log(n_evt)/math.log(2)) + 1

    entry_number_start = 0
    entry_number_stop  = 0

    for b in reversed(range(n_bits)):
        to_check = entry_number_start + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < start:
            entry_number_start = to_check
        to_check = entry_number_stop + (1<<b)
        if to_check < n_evt and db.get_entry(to_check)["date"] < stop:
            entry_number_stop = to_check


    read_every = 1
    while (entry_number_stop-entry_number_start)*1./read_every > max_points:
        read_every += 1

    entry_number = entry_number_start
    while(entry_number < entry_number_stop):
        entry = db.get_entry(entry_number)
        if not entry==None:
            for key in entry:
                val = entry[key]

                if key == "date":
                    data["date"].append(datetime.fromtimestamp(val).strftime("%Y-%m-%d %H:%M:%S"))

                data_type = "None"
                if  "LV" in key:
                    data_type = "lv_v"
                elif  "LI" in key:
                    data_type = "lv_i"
                elif  "HV" in key:
                    data_type = "hv_v"
                elif  "HI" in key:
                    data_type = "hv_i"
                if data_type == "None":
                    continue

                if not key in data[data_type]:
                    data[data_type][key] = [val_converter(data_type,val)]
                else:
                    data[data_type][key].append(val_converter(data_type,val))

        entry_number += read_every

    #creating plots from data :
    plots = {"lv_v" : [], "lv_i" : [],"hv_v" : [], "hv_i" : []}
    for meas_type in ["lv_v","lv_i","hv_v","hv_i"]:
        for meas in data[meas_type]:
            name = meas
            if meas in psu_naming.keys():
                name = psu_naming[meas]
            if name == "N/A":
                continue
            plots[meas_type].append({
                "y" : data[meas_type][meas],
                "x" : data["date"],
                "type" : "scatter",
                "name" : name
            })
    return plots


@app.route('/psu_control')
def psu_control_page():
    return render_template('psu_control_page.html')

@app.route('/get_live_psu')
def get_live_psu():
    data = {}
    data["1_status"] = rq.query("0 is_enabled 1")
    data["1_set_v"] =  float(rq.query("0 get_v_set 1"))
    data["1_mon_v"] =  float(rq.query("0 get_voltage 1"))
    data["1_mon_i"] =  float(rq.query("0 get_current 1"))
    data["2_status"] = rq.query("0 is_enabled 2")
    data["2_set_v"] =  float(rq.query("0 get_v_set 2"))
    data["2_mon_v"] =  float(rq.query("0 get_voltage 2"))
    data["2_mon_i"] =  float(rq.query("0 get_current 2"))
    data["3_status"] = rq.query("0 is_enabled 3")
    data["3_set_v"] =  float(rq.query("0 get_v_set 3"))
    data["3_mon_v"] =  float(rq.query("0 get_voltage 3"))
    data["3_mon_i"] =  float(rq.query("0 get_current 3"))
    return data

@app.route("/send_psu_cmd")
def send_psu_cmd():
    cmd = request.args.get('cmd')
    if cmd == None:
        return ("No command to process")
    
    cmd = cmd.split(" ")
    if cmd[0] in ["turn_on","turn_off"]:
        if len(cmd) == 2 and cmd[1].isdigit() and int(cmd[1]) in [1,2,3]:
            rq.query(f"0 {cmd[0]} {cmd[1]}")
    elif cmd[0] == "set_v":
        if len(cmd) == 3:
            try:
                channel = int(cmd[1])
                voltage = float(cmd[2])
                if channel in [1,2,3] and voltage >= 0 and voltage <= 15:
                    rq.query(f"0 set_v {voltage} {channel}")
            except:
                pass
                    
    return ""
